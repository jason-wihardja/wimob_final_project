package com.wimob.wirelesskite;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

public class LoginActivity extends ActionBarActivity {
    TextView title, forgetPassword;
    Button loginButton;
    EditText usernameEditText, passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().hide();

        setContentView(R.layout.activity_login);

        title = (TextView) findViewById(R.id.kiteStringTitle_login);
        title.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSans-Light.ttf"));

        loginButton = (Button) findViewById(R.id.LoginButton_login);
        loginButton.setOnClickListener(loginButtonClicked);

        usernameEditText = (EditText) findViewById(R.id.usernameEditText_login);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText_login);

        forgetPassword = (TextView) findViewById(R.id.forgetPasswordTextView);
        forgetPassword.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSans-Light.ttf"));
        forgetPassword.setOnClickListener(forgetPasswordClicked);
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void doLogin() {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, "Logging in...", true, false);

        ParseUser.logInInBackground(usernameEditText.getText().toString(), passwordEditText.getText().toString(), new LogInCallback() {

            @Override
            public void done(ParseUser user, ParseException e) {
                progressDialog.dismiss();

                if (user != null) {
                    proceedWithLogin();
                } else {
                    displayError(e);
                }
            }
        });
    }

    public void proceedWithLogin() {
        Intent intent = new Intent(this, ProgramActivity.class);
        startActivity(intent);
        finish();
    }

    OnClickListener loginButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Boolean errorExists = false;

            // Form Validation
            if (usernameEditText.getText().toString().trim().length() == 0) {
                usernameEditText.setError(getString(R.string.username_empty_error_message));
                errorExists = true;
            } else usernameEditText.setError(null);

            if (passwordEditText.getText().toString().trim().length() == 0) {
                passwordEditText.setError(getString(R.string.password_empty_error_message));
                errorExists = true;
            } else if (passwordEditText.getText().toString().trim().length() < 6) {
                passwordEditText.setError(getString(R.string.password_short_error_message));
                errorExists = true;
            } else passwordEditText.setError(null);

            // Do Action
            if (!errorExists) doLogin();
        }
    };

    OnClickListener forgetPasswordClicked = new OnClickListener() {

        @Override
        public void onClick(View v) {
            showForgetPasswordPrompt();
        }
    };

    public void showForgetPasswordPrompt() {
        AlertDialog.Builder prompt = new AlertDialog.Builder(this);
        prompt.setTitle("Please enter your email address");

        final EditText email = (EditText) getLayoutInflater().inflate(R.layout.email_prompt_layout, null);
        prompt.setView(email);

        prompt.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestPasswordReset(email.getText().toString());
            }
        });
        prompt.setNegativeButton("Cancel", null);

        prompt.show();
    }

    public void requestPasswordReset(String email) {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, "Requesting password reset...", true, false);

        ParseUser.requestPasswordResetInBackground(email, new RequestPasswordResetCallback() {

            @Override
            public void done(ParseException e) {
                progressDialog.dismiss();

                if (e == null) {
                    displaySuccess();
                } else {
                    displayError(e);
                }
            }
        });
    }

    private void displaySuccess() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Success!");
        alert.setIcon(R.drawable.ic_action_accept);

        String message = "An email was successfully sent with reset instructions. Please check your email.";
        alert.setMessage(message);

        alert.setNeutralButton("OK", null);
        alert.show();
    }

    private void displayError(ParseException e) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Error!");
        alert.setIcon(R.drawable.ic_action_error);

        String message = e.getMessage();
        message = Character.toUpperCase(message.charAt(0)) + message.substring(1);
        alert.setMessage(message);

        alert.setNeutralButton("OK", null);
        alert.show();
    }
}
