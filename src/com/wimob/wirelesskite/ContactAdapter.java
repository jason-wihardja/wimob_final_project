package com.wimob.wirelesskite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.widget.SimpleAdapter;

public class ContactAdapter extends SimpleAdapter {

    public ContactAdapter(Context context, JSONArray contactData, int resource, String[] from, int[] to) {
        super(context, convertJSONArrayToArrayList(contactData), resource, from, to);
    }

    private static ArrayList<HashMap<String, String>> convertJSONArrayToArrayList(JSONArray contactData) {
        ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();

        for (int i = 0; i < contactData.length(); i++) {
            try {
                HashMap<String, String> contact = new HashMap<String, String>();

                JSONObject currentContact = contactData.getJSONObject(i);
                contact.put("name", currentContact.getString("name"));
                contact.put("number", currentContact.getString("number"));

                result.add(contact);
            } catch (JSONException e) {
                Log.e("CONTACT_ADAPTER", e.getMessage());
            }
        }

        if (contactData.length() > 0) {
            Collections.sort(result, new Comparator<HashMap<String, String>>() {

                @Override
                public int compare(HashMap<String, String> lhs, HashMap<String, String> rhs) {
                    return lhs.get("name").compareTo(rhs.get("name"));
                }
            });
        }
        
        return result;
    }
}
