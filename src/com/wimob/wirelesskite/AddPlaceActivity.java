package com.wimob.wirelesskite;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

public class AddPlaceActivity extends ActionBarActivity {

    private final int animationDuration = 5000;

    private MapView mapView;
    private GoogleMap googleMap;
    private Double latitude, longitude;
    private int mapType;

    private ParseObject userPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_place);

        getSupportActionBar().setTitle("Add Common Place");
        getSupportActionBar().setIcon(R.drawable.maps_icon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState != null) mapType = savedInstanceState.getInt("mapType");
        else mapType = GoogleMap.MAP_TYPE_HYBRID;

        this.userPreference = ProgramActivity.getUserPreferences();

        Button roadMapButton = (Button) findViewById(R.id.mapStyleButton);
        roadMapButton.setOnClickListener(mapTypeButtonClicked);

        Button hybridButton = (Button) findViewById(R.id.hybridStyleButton);
        hybridButton.setOnClickListener(mapTypeButtonClicked);

        Button terrainButton = (Button) findViewById(R.id.terrainStyleButton);
        terrainButton.setOnClickListener(mapTypeButtonClicked);

        final ImageButton searchQueryButton = (ImageButton) findViewById(R.id.searchQueryButton);
        searchQueryButton.setOnClickListener(searchButtonClicked);

        final EditText searchQueryEditText = (EditText) findViewById(R.id.searchQueryEditText);
        searchQueryEditText.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) return searchQueryButton.performClick();
                return false;
            }
        });

        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        MapsInitializer.initialize(this);

        setupMapIfNeeded();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        
        if (googleMap != null) setupMap();

        if (googleMap == null) {
            googleMap = mapView.getMap();
            if (googleMap != null) setupMap();
        }
    }

    private void setupMapIfNeeded() {
        if (googleMap == null) {
            googleMap = mapView.getMap();
            if (googleMap != null) setupMap();
        }
    }

    private void setupMap() {
        googleMap.setMyLocationEnabled(true);
        googleMap.setBuildingsEnabled(true);
        googleMap.setIndoorEnabled(false);
        googleMap.getUiSettings().setAllGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setMapType(mapType);
        googleMap.setPadding(0, (int) getResources().getDimension(R.dimen.search_layout_size), 0, (int) getResources().getDimension(R.dimen.button_bar_height));
        googleMap.setOnMapLongClickListener(mapLongClicked);

        moveToMyLocation();
    }

    public void moveToMyLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location location = null;
        String[] providers = { LocationManager.GPS_PROVIDER, LocationManager.NETWORK_PROVIDER, LocationManager.PASSIVE_PROVIDER };
        for (String provider : providers) {
            if (!locationManager.isProviderEnabled(provider)) continue;
            if (locationManager != null) {
                location = locationManager.getLastKnownLocation(provider);
                if (location != null) break;
            }
        }

        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f), animationDuration, null);
        }
    }

    OnClickListener mapTypeButtonClicked = new OnClickListener() {

        @Override
        public void onClick(View view) {
            int previousType = mapType;

            switch (view.getId()) {
                case R.id.mapStyleButton:
                    mapType = GoogleMap.MAP_TYPE_NORMAL;
                    break;

                case R.id.hybridStyleButton:
                    mapType = GoogleMap.MAP_TYPE_HYBRID;
                    break;

                case R.id.terrainStyleButton:
                    mapType = GoogleMap.MAP_TYPE_TERRAIN;
                    break;
            }

            if (previousType != mapType) googleMap.setMapType(mapType);
        }
    };

    OnClickListener searchButtonClicked = new OnClickListener() {
        final String LatLngRegex = "([-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?))(,\\s*)([-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?))";
        final Pattern pattern = Pattern.compile(LatLngRegex);

        @Override
        public void onClick(View v) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

            EditText searchQueryEditText = (EditText) findViewById(R.id.searchQueryEditText);
            String searchQuery = searchQueryEditText.getText().toString();

            if (searchQuery.length() > 0) {
                final Matcher matcher = pattern.matcher(searchQuery);

                if (matcher.matches()) {
                    latitude = Double.parseDouble(matcher.group(1));
                    longitude = Double.parseDouble(matcher.group(6));

                    LatLng latLng = new LatLng(latitude, longitude);

                    googleMap.clear();
                    googleMap.addMarker(new MarkerOptions().position(latLng).title("Destination").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).draggable(false));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng), animationDuration, null);
                } else {
                    new GeocodeLoadTask(AddPlaceActivity.this).execute(searchQuery);
                }
            }
        }
    };

    OnMapLongClickListener mapLongClicked = new OnMapLongClickListener() {

        @Override
        public void onMapLongClick(LatLng location) {
            EditText searchQueryEditText = (EditText) findViewById(R.id.searchQueryEditText);
            searchQueryEditText.setText(location.latitude + ", " + location.longitude);
            
            ImageButton searchQueryButton = (ImageButton) findViewById(R.id.searchQueryButton);
            searchQueryButton.performClick();
        }
    };

    private void addPlace() {
        final String LatLngRegex = "([-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?))(,\\s*)([-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?))";
        final Pattern pattern = Pattern.compile(LatLngRegex);

        EditText searchQueryEditText = (EditText) findViewById(R.id.searchQueryEditText);
        String searchQuery = searchQueryEditText.getText().toString();

        final Matcher matcher = pattern.matcher(searchQuery);

        JSONArray placeListJSON = userPreference.getJSONArray("common_places");

        try {
            String placeName;

            if (matcher.matches()) placeName = latitude.toString() + "," + longitude.toString();
            else placeName = Character.toUpperCase(searchQuery.charAt(0)) + searchQuery.substring(1);

            JSONObject newPlace = new JSONObject();

            newPlace.put("name", placeName);
            newPlace.put("placeQuery", latitude.toString() + "," + longitude.toString());
            newPlace.put("zoomLevel", 17);

            placeListJSON.put(newPlace);
            userPreference.put("common_places", placeListJSON);
            savePreference();
        } catch (JSONException e) {
            Log.e("ADD_PLACE_ACTIVITY", e.getMessage());
            Toast.makeText(this, "Failed to save new location data.", Toast.LENGTH_SHORT).show();
        }
    }

    private void savePreference() {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, getString(R.string.save_changes_string), true, false);

        userPreference.saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {
                if (e == null) {
                    progressDialog.dismiss();
                    finish();
                } else {
                    displayError(e);
                }
            }
        });
    }

    private void displayError(ParseException e) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Error!");
        alert.setIcon(R.drawable.ic_action_error);

        String message = e.getMessage();
        message = Character.toUpperCase(message.charAt(0)) + message.substring(1);
        alert.setMessage(message);

        alert.setPositiveButton("OK", null);
        alert.show();
    }

    private class GeocodeLoadTask extends AsyncTask<String, Void, LatLng> {

        private Context context;

        private String locationName;
        private ProgressDialog progressDialog;

        public GeocodeLoadTask(Context context) {
            super();
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(context, null, "Searching...", true, false);
        }

        @Override
        protected LatLng doInBackground(String... params) {
            locationName = params[0];

            Geocoder geocoder = new Geocoder(context);
            try {
                List<Address> results = geocoder.getFromLocationName(locationName, 1);
                if (results.size() == 0) {
                    return null;
                } else {
                    return new LatLng(results.get(0).getLatitude(), results.get(0).getLongitude());
                }
            } catch (IOException e) {
                Log.e("GEOCODE_LOAD_TASK", e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(LatLng result) {
            progressDialog.dismiss();

            if (result != null) {
                latitude = result.latitude;
                longitude = result.longitude;

                googleMap.clear();
                googleMap.addMarker(new MarkerOptions().position(result).title("Destination").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).draggable(false));
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(result), animationDuration, null);
            } else {
                Toast.makeText(context, "Location not found!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                EditText searchQueryEditText = (EditText) findViewById(R.id.searchQueryEditText);
                String searchQuery = searchQueryEditText.getText().toString();

                if (searchQuery.length() > 0) {
                    addPlace();
                }

                break;
        }

        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("mapType", mapType);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onBackPressed() {
        System.gc();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
        mapView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
        setupMapIfNeeded();
        mapView.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
}
