package com.wimob.wirelesskite;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

public class AboutActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().hide();

        setContentView(R.layout.activity_about);

        TextView title = (TextView) findViewById(R.id.kiteStringTitle_about);
        title.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSans-Light.ttf"));
        
        TextView aboutContent = (TextView) findViewById(R.id.aboutContentTextView);
        String content = "Version 1.0";
        content += System.getProperty("line.separator");
        content += System.getProperty("line.separator");
        content += "Wireless Mobile Software Engineering";
        content += System.getProperty("line.separator");
        content += "Final Project";
        content += System.getProperty("line.separator");
        content += System.getProperty("line.separator");
        content += "Created by:";
        content += System.getProperty("line.separator");
        content += "Jason Wihardja";
        content += System.getProperty("line.separator");
        content += "1501198524";
        aboutContent.setText(content);
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
