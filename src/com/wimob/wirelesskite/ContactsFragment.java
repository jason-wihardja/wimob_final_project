package com.wimob.wirelesskite;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

public class ContactsFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ListView contactListView = (ListView) getActivity().findViewById(R.id.contactsListView);

        View emptyView = new View(getActivity());
        float density = getResources().getDisplayMetrics().density;
        int paddingDP = (int) (5 * density);
        emptyView.setMinimumHeight(paddingDP);

        contactListView.addHeaderView(emptyView, null, false);
        contactListView.addFooterView(emptyView, null, false);

        contactListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView contactNameTextView = (TextView) view.findViewById(R.id.contactName);
                TextView contactNumberTextView = (TextView) view.findViewById(R.id.contactNumber);
                showEditDialog(contactNameTextView.getText().toString(), contactNumberTextView.getText().toString());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        setupListView();
    }

    @Override
    public void onStop() {
        super.onStop();
        System.gc();
    }

    private void setupListView() {
        ListView contactListView = (ListView) getActivity().findViewById(R.id.contactsListView);
        JSONArray contactList = ProgramActivity.getUserPreferences().getJSONArray("emergency_contact_list");

        String from[] = { "name", "number" };
        int to[] = { R.id.contactName, R.id.contactNumber };
        ListAdapter listAdapter = new ContactAdapter(getActivity(), contactList, R.layout.contact_entry_layout, from, to);
        contactListView.setAdapter(listAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Emergency Contact");

        final TableLayout dialogContent = (TableLayout) getActivity().getLayoutInflater().inflate(R.layout.dialog_add_contact, null);
        final EditText name = (EditText) dialogContent.findViewById(R.id.nameEditText);
        final EditText phone = (EditText) dialogContent.findViewById(R.id.phoneEditText);

        builder.setView(dialogContent);
        builder.setPositiveButton("Save", null);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button okButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        okButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (name.getText().toString().length() > 0 && phone.getText().toString().length() > 0) {
                    ParseObject userPreferences = ProgramActivity.getUserPreferences();
                    JSONArray contactList = userPreferences.getJSONArray("emergency_contact_list");

                    try {
                        JSONObject newContact = new JSONObject();
                        newContact.put("name", name.getText().toString());
                        newContact.put("number", phone.getText().toString());
                        contactList.put(newContact);

                        alertDialog.dismiss();

                        userPreferences.put("emergency_contact_list", contactList);
                        savePreferences(userPreferences);
                    } catch (JSONException e) {
                        Log.e("CONTACTS_FRAGMENT", e.getMessage());
                    }
                } else {
                    Toast.makeText(getActivity(), "Invalid data", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return true;
    }

    private void showEditDialog(final String name, final String number) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Edit Emergency Contact");

        final TableLayout dialogContent = (TableLayout) getActivity().getLayoutInflater().inflate(R.layout.dialog_add_contact, null);
        final EditText nameET = (EditText) dialogContent.findViewById(R.id.nameEditText);
        nameET.setText(name);
        final EditText phoneET = (EditText) dialogContent.findViewById(R.id.phoneEditText);
        phoneET.setText(number);

        builder.setView(dialogContent);
        builder.setPositiveButton("Save", null);
        builder.setNegativeButton("Delete", null);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button saveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        saveButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (nameET.getText().toString().length() > 0 && phoneET.getText().toString().length() > 0) {
                    ParseObject userPreferences = ProgramActivity.getUserPreferences();
                    JSONArray oldContactList = userPreferences.getJSONArray("emergency_contact_list");

                    for (int i = 0; i < oldContactList.length(); i++) {
                        try {
                            if (name == oldContactList.getJSONObject(i).getString("name") && number == oldContactList.getJSONObject(i).getString("number")) {
                                JSONArray newContactList = new JSONArray();
                                for (int j = 0; j < oldContactList.length(); j++) {
                                    if (i == j) {
                                        JSONObject newContact = new JSONObject();
                                        newContact.put("name", nameET.getText().toString());
                                        newContact.put("number", phoneET.getText().toString());
                                        newContactList.put(newContact);
                                    } else {
                                        newContactList.put(oldContactList.getJSONObject(j));
                                    }
                                }

                                alertDialog.dismiss();

                                userPreferences.put("emergency_contact_list", newContactList);
                                savePreferences(userPreferences);

                                break;
                            }
                        } catch (JSONException e) {
                            Log.e("CONTACTS_FRAGMENT", e.getMessage());
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), "Invalid data", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button deleteButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        deleteButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                ParseObject userPreferences = ProgramActivity.getUserPreferences();
                JSONArray oldContactList = userPreferences.getJSONArray("emergency_contact_list");
                JSONArray newContactList = new JSONArray();

                for (int i = 0; i < oldContactList.length(); i++) {
                    try {
                        if (name != oldContactList.getJSONObject(i).getString("name") && number != oldContactList.getJSONObject(i).getString("number")) {
                            newContactList.put(oldContactList.getJSONObject(i));
                        }
                    } catch (JSONException e) {
                        Log.e("CONTACTS_FRAGMENT", e.getMessage());
                    }
                }

                alertDialog.dismiss();

                userPreferences.put("emergency_contact_list", newContactList);
                savePreferences(userPreferences);
            }
        });
    }

    private void savePreferences(ParseObject preference) {
        final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), null, getString(R.string.save_changes_string), true, false);

        preference.saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {
                progressDialog.dismiss();

                if (e == null) {
                    setupListView();
                } else {
                    displayError(e);
                }
            }
        });
    }

    private void displayError(ParseException e) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

        alert.setTitle("Error!");
        alert.setIcon(R.drawable.ic_action_error);

        String message = e.getMessage();
        message = Character.toUpperCase(message.charAt(0)) + message.substring(1);
        alert.setMessage(message);

        alert.setPositiveButton("OK", null);
        alert.show();
    }
}
