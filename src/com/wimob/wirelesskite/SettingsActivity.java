package com.wimob.wirelesskite;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.TextView;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

@SuppressLint("InflateParams")
public class SettingsActivity extends ActionBarActivity {

    private ParseObject userPreference;

    private Integer checkInterval;
    private Integer maxInactiveTime;
    private String emergencyMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setTitle(R.string.settings_string);
        getSupportActionBar().setIcon(R.drawable.settings_icon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_settings);

        this.userPreference = ProgramActivity.getUserPreferences();
        this.checkInterval = userPreference.getInt("check_interval");
        this.maxInactiveTime = userPreference.getInt("inactive_interval");
        this.emergencyMessage = userPreference.getString("emergency_message");

        TextView checkIntervalTextView = (TextView) findViewById(R.id.checkIntervalTextView);
        checkIntervalTextView.setOnClickListener(textViewClicked);

        TextView inactiveTimeTextView = (TextView) findViewById(R.id.inactiveTimeTextView);
        inactiveTimeTextView.setOnClickListener(textViewClicked);

        TextView emergencyMessageTextView = (TextView) findViewById(R.id.emergencyMessageTextView);
        emergencyMessageTextView.setOnClickListener(textViewClicked);
    }

    private View.OnClickListener textViewClicked = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            final AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());

            switch (view.getId()) {
                case R.id.checkIntervalTextView:
                    final LinearLayout checkIntervalLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.check_interval_setting_layout, null);

                    dialog.setTitle(R.string.check_interval_string);
                    dialog.setView(checkIntervalLayout);
                    dialog.setCancelable(true);

                    final NumberPicker intervalNumberPicker = (NumberPicker) checkIntervalLayout.findViewById(R.id.intervalNumberPicker);
                    intervalNumberPicker.setMinValue(maxInactiveTime + 1);
                    intervalNumberPicker.setMaxValue(9999);
                    intervalNumberPicker.setValue(checkInterval);
                    intervalNumberPicker.setWrapSelectorWheel(false);

                    dialog.setPositiveButton(R.string.save_string, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            checkInterval = intervalNumberPicker.getValue();
                        }
                    });

                    dialog.show();
                    break;

                case R.id.inactiveTimeTextView:
                    final LinearLayout maxInactiveIntervalLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.max_inactive_setting_layout, null);

                    dialog.setTitle(R.string.inactive_time_string);
                    dialog.setView(maxInactiveIntervalLayout);
                    dialog.setCancelable(true);

                    final NumberPicker maxInactiveNumberPicker = (NumberPicker) maxInactiveIntervalLayout.findViewById(R.id.maxInactiveNumberPicker);
                    maxInactiveNumberPicker.setMinValue(1);
                    maxInactiveNumberPicker.setMaxValue(checkInterval - 1);
                    maxInactiveNumberPicker.setValue(maxInactiveTime);
                    maxInactiveNumberPicker.setWrapSelectorWheel(false);

                    dialog.setPositiveButton(R.string.save_string, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            maxInactiveTime = maxInactiveNumberPicker.getValue();
                        }
                    });

                    dialog.show();
                    break;

                case R.id.emergencyMessageTextView:
                    final ScrollView emergencyMessageLayout = (ScrollView) getLayoutInflater().inflate(R.layout.emergency_message_setting_layout, null);

                    dialog.setTitle(R.string.emergency_message_string);
                    dialog.setView(emergencyMessageLayout);
                    dialog.setCancelable(true);

                    final EditText emergencyMessageContent = (EditText) emergencyMessageLayout.findViewById(R.id.emergencyMessageContent);
                    emergencyMessageContent.setText(emergencyMessage);

                    dialog.setPositiveButton(R.string.save_string, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            emergencyMessage = emergencyMessageContent.getText().toString();
                        }
                    });

                    dialog.show();
                    break;
            }
        }
    };

    private void savePreferences() {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, getString(R.string.save_changes_string), true, false);

        userPreference.put("check_interval", checkInterval);
        userPreference.put("inactive_interval", maxInactiveTime);
        userPreference.put("emergency_message", emergencyMessage);

        userPreference.saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {
                progressDialog.dismiss();

                if (e == null) {
                    finish();
                } else {
                    displayError(e);
                }
            }
        });
    }

    private void displayError(ParseException e) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Error!");
        alert.setIcon(R.drawable.ic_action_error);

        String message = e.getMessage();
        message = Character.toUpperCase(message.charAt(0)) + message.substring(1);
        alert.setMessage(message);

        alert.setPositiveButton("OK", null);
        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
    }

    @Override
    public void onBackPressed() {
        savePreferences();
    }
}
