package com.wimob.wirelesskite;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class HomeFragment extends Fragment {

    private final int animationDuration = 5000;

    private MapView mapView;
    private GoogleMap googleMap;
    private Double latitude, longitude;
    private int mapType;

    private LatLng latLng;

    private Button goButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if (savedInstanceState != null) mapType = savedInstanceState.getInt("mapType");
        else mapType = GoogleMap.MAP_TYPE_HYBRID;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        TextView welcome = (TextView) view.findViewById(R.id.welcomeTextView);
        welcome.setTypeface(Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(), "OpenSans-Italic.ttf"));

        final ImageButton searchButton = (ImageButton) view.findViewById(R.id.searchButton);
        searchButton.setOnClickListener(searchButtonClicked);

        goButton = (Button) view.findViewById(R.id.goButton);
        goButton.setOnClickListener(goButtonClicked);
        goButton.setEnabled(false);

        final EditText searchQueryEditText = (EditText) view.findViewById(R.id.searchLocationEditText);
        searchQueryEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                goButton.setEnabled(false);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Do nothing
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Do nothing
            }
        });
        searchQueryEditText.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) return searchButton.performClick();
                return false;
            }
        });

        Button roadMapButton = (Button) view.findViewById(R.id.roadMapButton);
        roadMapButton.setOnClickListener(mapTypeButtonClicked);

        Button hybridButton = (Button) view.findViewById(R.id.hybridButton);
        hybridButton.setOnClickListener(mapTypeButtonClicked);

        Button terrainButton = (Button) view.findViewById(R.id.terrainButton);
        terrainButton.setOnClickListener(mapTypeButtonClicked);

        mapView = (MapView) view.findViewById(R.id.searchLocationMap);
        mapView.onCreate(savedInstanceState);
        MapsInitializer.initialize(getActivity());

        setupMapIfNeeded();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (googleMap != null) setupMap();

        if (googleMap == null) {
            googleMap = mapView.getMap();
            if (googleMap != null) setupMap();
        }
    }

    private void setupMapIfNeeded() {
        if (googleMap == null) {
            googleMap = mapView.getMap();
            if (googleMap != null) setupMap();
        }
    }

    private void setupMap() {
        googleMap.setMyLocationEnabled(true);
        googleMap.setBuildingsEnabled(true);
        googleMap.setIndoorEnabled(false);
        googleMap.getUiSettings().setAllGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setMapType(mapType);
        googleMap.setPadding(0, (int) getResources().getDimension(R.dimen.button_bar_height), 0, 0);
        googleMap.setOnMapLongClickListener(mapLongClicked);

        moveToMyLocation();
    }

    public void moveToMyLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Location location = null;
        String[] providers = { LocationManager.GPS_PROVIDER, LocationManager.NETWORK_PROVIDER, LocationManager.PASSIVE_PROVIDER };
        for (String provider : providers) {
            if (!locationManager.isProviderEnabled(provider)) continue;
            if (locationManager != null) {
                location = locationManager.getLastKnownLocation(provider);
                if (location != null) break;
            }
        }

        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f), animationDuration, null);
        }
    }

    OnClickListener searchButtonClicked = new OnClickListener() {
        final String LatLngRegex = "([-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?))(,\\s*)([-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?))";
        final Pattern pattern = Pattern.compile(LatLngRegex);

        @Override
        public void onClick(View view) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

            EditText searchEditText = (EditText) getActivity().findViewById(R.id.searchLocationEditText);
            String searchQuery = searchEditText.getText().toString();

            if (searchQuery.length() > 0) {
                final Matcher matcher = pattern.matcher(searchQuery);

                if (matcher.matches()) {
                    latitude = Double.parseDouble(matcher.group(1));
                    longitude = Double.parseDouble(matcher.group(6));

                    latLng = new LatLng(latitude, longitude);

                    googleMap.clear();
                    googleMap.addMarker(new MarkerOptions().position(latLng).title("Destination").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).draggable(false));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng), animationDuration, null);

                    goButton.setEnabled(true);
                } else {
                    new GeocodeLoadTask().execute(searchQuery);
                }
            }
        }
    };

    OnClickListener goButtonClicked = new OnClickListener() {
        final String LatLngRegex = "([-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?))(,\\s*)([-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?))";
        final Pattern pattern = Pattern.compile(LatLngRegex);
        
        @Override
        public void onClick(View v) {
            EditText searchQueryEditText = (EditText) getActivity().findViewById(R.id.searchLocationEditText);

            if (pattern.matcher(searchQueryEditText.getText()).matches()) {
                Intent intent = new Intent(getActivity(), TripActivity.class);
                intent.putExtra("destination", latLng.latitude + ", " + latLng.longitude);
                intent.putExtra("coordinate", latLng.latitude + ", " + latLng.longitude);
                startActivity(intent);
            } else {
                Intent intent = new Intent(getActivity(), TripActivity.class);
                intent.putExtra("destination", searchQueryEditText.getText().toString());
                intent.putExtra("coordinate", latLng.latitude + ", " + latLng.longitude);
                startActivity(intent);
            }
        }
    };

    OnClickListener mapTypeButtonClicked = new OnClickListener() {

        @Override
        public void onClick(View view) {
            int previousType = mapType;

            switch (view.getId()) {
                case R.id.roadMapButton:
                    mapType = GoogleMap.MAP_TYPE_NORMAL;
                    break;

                case R.id.hybridButton:
                    mapType = GoogleMap.MAP_TYPE_HYBRID;
                    break;

                case R.id.terrainButton:
                    mapType = GoogleMap.MAP_TYPE_TERRAIN;
                    break;
            }

            if (previousType != mapType) googleMap.setMapType(mapType);
        }
    };

    OnMapLongClickListener mapLongClicked = new OnMapLongClickListener() {

        @Override
        public void onMapLongClick(LatLng location) {
            EditText searchQueryEditText = (EditText) getActivity().findViewById(R.id.searchLocationEditText);
            searchQueryEditText.setText(location.latitude + ", " + location.longitude);

            ImageButton searchButton = (ImageButton) getActivity().findViewById(R.id.searchButton);
            searchButton.performClick();
        }
    };

    private class GeocodeLoadTask extends AsyncTask<String, Void, LatLng> {

        private String locationName;
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getActivity(), null, "Searching...", true, false);
        }

        @Override
        protected LatLng doInBackground(String... params) {
            locationName = params[0];

            Geocoder geocoder = new Geocoder(getActivity());
            try {
                List<Address> results = geocoder.getFromLocationName(locationName, 1);
                if (results.size() == 0) {
                    return null;
                } else {
                    return new LatLng(results.get(0).getLatitude(), results.get(0).getLongitude());
                }
            } catch (IOException e) {
                Log.e("GEOCODE_LOAD_TASK", e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(LatLng result) {
            progressDialog.dismiss();
            latLng = result;

            if (result != null) {
                goButton.setEnabled(true);

                googleMap.clear();
                googleMap.addMarker(new MarkerOptions().position(result).title("Destination").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).draggable(false));
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(result), animationDuration, null);
            } else {
                Toast.makeText(getActivity(), "Location not found!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("mapType", mapType);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        System.gc();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
}
