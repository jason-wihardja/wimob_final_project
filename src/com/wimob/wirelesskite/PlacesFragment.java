package com.wimob.wirelesskite;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

public class PlacesFragment extends Fragment {

    private ParseObject userPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        userPreferences = ProgramActivity.getUserPreferences();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_places, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ListView placesListView = (ListView) getActivity().findViewById(R.id.placesListView);

        View emptyView = new View(getActivity());
        float density = getResources().getDisplayMetrics().density;
        int paddingDP = (int) (5 * density);
        emptyView.setMinimumHeight(paddingDP);

        placesListView.addHeaderView(emptyView, null, false);
        placesListView.addFooterView(emptyView, null, false);

        placesListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final TextView name = (TextView) view.findViewById(R.id.placeName);
                final TextView placeQuery = (TextView) view.findViewById(R.id.placeQuery);
                final TextView zoomLevel = (TextView) view.findViewById(R.id.zoomLevel);

                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.navigate_delete_dialog);
                dialog.setTitle(name.getText().toString());

                LinearLayout navigateLayout = (LinearLayout) dialog.findViewById(R.id.navigateLayout);
                navigateLayout.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), TripActivity.class);
                        intent.putExtra("destination", name.getText().toString());
                        intent.putExtra("coordinate", placeQuery.getText().toString());
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });

                LinearLayout deleteLayout = (LinearLayout) dialog.findViewById(R.id.deleteLayout);
                deleteLayout.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        deletePlace(name.getText().toString(), placeQuery.getText().toString(), zoomLevel.getText().toString());
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        setupListView();
    }

    public void setupListView() {
        ListView placesListView = (ListView) getActivity().findViewById(R.id.placesListView);

        ArrayList<Place> places = new ArrayList<Place>();
        JSONArray placeListJSON = ProgramActivity.getUserPreferences().getJSONArray("common_places");
        for (int i = 0; i < placeListJSON.length(); i++) {
            try {
                Place newPlace = new Place(getActivity().getApplicationContext(), (JSONObject) placeListJSON.get(i));
                places.add(newPlace);
            } catch (JSONException e) {
                Toast.makeText(getActivity().getApplicationContext(), "Failed to retrieve common places list.", Toast.LENGTH_SHORT).show();
            }

            Collections.sort(places);
        }

        PlacesAdapter placesAdapter = new PlacesAdapter(getActivity(), places);
        placesListView.setAdapter(placesAdapter);
    }

    private void deletePlace(String placeName, String placeQuery, String zoomLevel) {
        JSONArray oldPlaceList = userPreferences.getJSONArray("common_places");
        JSONArray newPlaceList = new JSONArray();

        for (int i = 0; i < oldPlaceList.length(); i++) {
            try {
                if (placeName != oldPlaceList.getJSONObject(i).getString("name") && placeQuery != oldPlaceList.getJSONObject(i).getString("placeQuery")) {
                    newPlaceList.put(oldPlaceList.getJSONObject(i));
                }
            } catch (JSONException e) {
                Log.e("PLACES_FRAGMENT", e.getMessage());
            }
        }
        
        userPreferences.put("common_places", newPlaceList);
        saveUserPreference();
    }
    
    private void saveUserPreference() {
        final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), null, getString(R.string.save_changes_string), true, false);
        
        userPreferences.saveInBackground(new SaveCallback() {
            
            @Override
            public void done(ParseException e) {
                progressDialog.dismiss();
                
                if (e == null) {
                    System.gc();
                    setupListView();
                } else {
                    displayError(e);
                }
            }
        });
    }
    
    private void displayError(ParseException e) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

        alert.setTitle("Error!");
        alert.setIcon(R.drawable.ic_action_error);

        String message = e.getMessage();
        message = Character.toUpperCase(message.charAt(0)) + message.substring(1);
        alert.setMessage(message);

        alert.setPositiveButton("OK", null);
        alert.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        setupListView();
    }

    @Override
    public void onStop() {
        super.onStop();
        System.gc();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(getActivity(), AddPlaceActivity.class);
        startActivity(intent);
        return true;
    }
}
