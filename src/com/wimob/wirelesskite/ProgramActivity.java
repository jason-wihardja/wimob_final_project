package com.wimob.wirelesskite;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

@SuppressLint("DefaultLocale")
@SuppressWarnings("deprecation")
public class ProgramActivity extends ActionBarActivity {

    private String title;
    private int iconRes;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;

    private static ParseUser currentUser;
    private static ParseObject userPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program);

        ImageView profilePicture = (ImageView) findViewById(R.id.profilePicture);
        profilePicture.setImageBitmap(generateRoundCorner(BitmapFactory.decodeResource(getResources(), R.drawable.default_user_pic), 50, 16, getApplicationContext()));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.string.app_name, R.string.app_name) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                supportInvalidateOptionsMenu();
            }

        };
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        LinearLayout home = (LinearLayout) findViewById(R.id.homeLinearLayout);
        home.setOnClickListener(navigationDrawerItemClicked);

        LinearLayout commonPlaces = (LinearLayout) findViewById(R.id.placesLinearLayout);
        commonPlaces.setOnClickListener(navigationDrawerItemClicked);

        LinearLayout emergencyContacts = (LinearLayout) findViewById(R.id.contactsLinearLayout);
        emergencyContacts.setOnClickListener(navigationDrawerItemClicked);

        LinearLayout settings = (LinearLayout) findViewById(R.id.settingsLinearLayout);
        settings.setOnClickListener(navigationDrawerItemClicked);

        LinearLayout about = (LinearLayout) findViewById(R.id.aboutLinearLayout);
        about.setOnClickListener(navigationDrawerItemClicked);

        LinearLayout logout = (LinearLayout) findViewById(R.id.logoutLinearLayout);
        logout.setOnClickListener(navigationDrawerItemClicked);

        TextView settingsTextView = (TextView) findViewById(R.id.textView4);
        settingsTextView.setText(settingsTextView.getText().toString().toUpperCase());

        TextView aboutTextView = (TextView) findViewById(R.id.textView5);
        aboutTextView.setText(aboutTextView.getText().toString().toUpperCase());

        TextView logoutTextView = (TextView) findViewById(R.id.textView6);
        logoutTextView.setText(logoutTextView.getText().toString().toUpperCase());

        TextView titleBottom = (TextView) findViewById(R.id.textView7);
        titleBottom.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSans-Light.ttf"));
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();

        if (savedInstanceState == null) findViewById(R.id.homeLinearLayout).performClick();
        else {
            title = savedInstanceState.getString("title");
            iconRes = (int) savedInstanceState.getLong("iconRes");
            setTitleAndIcon();
        }

        final ProgressDialog progressDialog = ProgressDialog.show(this, null, "Synchronizing user data...", true, false);

        currentUser = ParseUser.getCurrentUser();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Preferences").whereEqualTo("user_id", currentUser);
        query.getFirstInBackground(new GetCallback<ParseObject>() {

            @Override
            public void done(ParseObject object, ParseException e) {
                progressDialog.dismiss();

                if (object != null) {
                    userPreferences = object;
                    initializeUI();
                } else {
                    displayError(e);
                    finish();
                }
            }
        });
    }

    private void initializeUI() {
        TextView usernameTextView = (TextView) findViewById(R.id.userNameTextView);
        usernameTextView.setText(currentUser.getUsername());

        TextView emailTextView = (TextView) findViewById(R.id.userEmailTextView);
        emailTextView.setText(currentUser.getEmail());
    }

    private void setTitleAndIcon() {
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setIcon(iconRes);
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
        drawerLayout.closeDrawers();
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("title", title);
        outState.putLong("iconRes", iconRes);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setVisible(!drawerLayout.isDrawerOpen(GravityCompat.START));
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) return true;
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent e) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.openDrawer(GravityCompat.START);
            } else {
                drawerLayout.closeDrawers();
            }
            return true;
        }
        return super.onKeyDown(keyCode, e);
    }

    private void displayError(ParseException e) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Error!");
        alert.setIcon(R.drawable.ic_action_error);

        String message = e.getMessage();
        message = Character.toUpperCase(message.charAt(0)) + message.substring(1);
        alert.setMessage(message);

        alert.setPositiveButton("OK", null);
        alert.show();
    }

    OnClickListener navigationDrawerItemClicked = new OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.homeLinearLayout:
                    if (!getSupportActionBar().getTitle().toString().equals(getString(R.string.home_string))) {
                        Fragment fragment = new HomeFragment();
                        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.fragment_open_animation, R.anim.fragment_close_animation).replace(R.id.contentLayout, fragment).commit();

                        title = getString(R.string.home_string);
                        iconRes = R.drawable.home_icon;
                        setTitleAndIcon();

                        updateNavigationDrawerItems(R.id.homeLinearLayout);
                    }
                    drawerLayout.closeDrawers();
                    break;

                case R.id.placesLinearLayout:
                    if (!getSupportActionBar().getTitle().toString().equals(getString(R.string.places_string))) {
                        Fragment fragment = new PlacesFragment();
                        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.fragment_open_animation, R.anim.fragment_close_animation).replace(R.id.contentLayout, fragment).commit();

                        title = getString(R.string.places_string);
                        iconRes = R.drawable.places_icon;
                        setTitleAndIcon();

                        updateNavigationDrawerItems(R.id.placesLinearLayout);
                    }
                    drawerLayout.closeDrawers();
                    break;

                case R.id.contactsLinearLayout:
                    if (!getSupportActionBar().getTitle().toString().equals(getString(R.string.contacts_string))) {
                        Fragment fragment = new ContactsFragment();
                        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.fragment_open_animation, R.anim.fragment_close_animation).replace(R.id.contentLayout, fragment).commit();

                        title = getString(R.string.contacts_string);
                        iconRes = R.drawable.contacts_icon;
                        setTitleAndIcon();

                        updateNavigationDrawerItems(R.id.contactsLinearLayout);
                    }
                    drawerLayout.closeDrawers();
                    break;

                case R.id.settingsLinearLayout:
                    startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                    break;

                case R.id.aboutLinearLayout:
                    startActivity(new Intent(getApplicationContext(), AboutActivity.class));
                    break;

                case R.id.logoutLinearLayout:
                    ParseUser.logOut();
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                    break;
            }
        }
    };

    private void updateNavigationDrawerItems(int viewID) {
        if (viewID == R.id.homeLinearLayout) {
            LinearLayout layout = (LinearLayout) findViewById(R.id.homeLinearLayout);
            layout.setBackgroundResource(R.drawable.list_item_bg_pressed);
        } else {
            LinearLayout layout = (LinearLayout) findViewById(R.id.homeLinearLayout);
            layout.setBackgroundResource(R.drawable.list_item_bg_normal);
        }

        if (viewID == R.id.placesLinearLayout) {
            LinearLayout layout = (LinearLayout) findViewById(R.id.placesLinearLayout);
            layout.setBackgroundResource(R.drawable.list_item_bg_pressed);
        } else {
            LinearLayout layout = (LinearLayout) findViewById(R.id.placesLinearLayout);
            layout.setBackgroundResource(R.drawable.list_item_bg_normal);
        }

        if (viewID == R.id.contactsLinearLayout) {
            LinearLayout layout = (LinearLayout) findViewById(R.id.contactsLinearLayout);
            layout.setBackgroundResource(R.drawable.list_item_bg_pressed);
        } else {
            LinearLayout layout = (LinearLayout) findViewById(R.id.contactsLinearLayout);
            layout.setBackgroundResource(R.drawable.list_item_bg_normal);
        }
    }

    private Bitmap generateRoundCorner(Bitmap src, int cornerDips, int borderDips, Context context) {
        Bitmap result = Bitmap.createBitmap(src.getWidth(), src.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);

        final int borderSizePx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) borderDips, context.getResources().getDisplayMetrics());
        final int cornerSizePx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) cornerDips, context.getResources().getDisplayMetrics());
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, src.getWidth(), src.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        paint.setColor(Color.WHITE);

        paint.setStyle(Style.FILL);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawRoundRect(rectF, cornerSizePx, cornerSizePx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(src, rect, rect, paint);

        paint.setStyle(Style.STROKE);
        paint.setStrokeWidth((float) borderSizePx);
        canvas.drawRoundRect(rectF, cornerSizePx, cornerSizePx, paint);

        return result;
    }

    public FragmentManager getCurrentFragmentManager() {
        return getSupportFragmentManager();
    }

    public static ParseUser getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(ParseUser currentUser) {
        ProgramActivity.currentUser = currentUser;
    }

    public static ParseObject getUserPreferences() {
        return userPreferences;
    }

    public static void setUserPreferences(ParseObject userPreferences) {
        ProgramActivity.userPreferences = userPreferences;
    }
}
