package com.wimob.wirelesskite;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.parse.Parse;
import com.parse.ParseUser;

public class MainActivity extends ActionBarActivity {
    ImageView mainLogo;
    Button loginButton, signUpButton;
    Animation logoAnimation, buttonAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().hide();

        setContentView(R.layout.activity_main);
        
        Parse.initialize(this, "6gqdDOISVqFnK7uis14pKuHtSGdAQTTW1TOaPbFh", "nZinEjWMNirlXUiqZxPqsYxZ7g3jtdbtYRJa3WWp");

        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser != null) {
            startActivity(new Intent(getApplicationContext(), ProgramActivity.class));
            finish();
        }
        
        mainLogo = (ImageView) findViewById(R.id.kitestringLogoImageView);
        logoAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.main_logo_animation);

        loginButton = (Button) findViewById(R.id.loginButton_main);
        signUpButton = (Button) findViewById(R.id.signUpButton_main);
        buttonAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.main_button_animation);
        buttonAnimation.setAnimationListener(buttonAnimationListener);
        
        mainLogo.startAnimation(logoAnimation);
        loginButton.startAnimation(buttonAnimation);
        signUpButton.startAnimation(buttonAnimation);
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    AnimationListener buttonAnimationListener = new AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
            loginButton.setOnClickListener(null);
            signUpButton.setOnClickListener(null);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            // Do nothing
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            loginButton.setOnClickListener(buttonClick);
            signUpButton.setOnClickListener(buttonClick);
        }
    };

    OnClickListener buttonClick = new OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loginButton_main:
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                    break;

                case R.id.signUpButton_main:
                    startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
                    finish();
                    break;

                default:
                    mainLogo.startAnimation(logoAnimation);
                    loginButton.startAnimation(buttonAnimation);
                    signUpButton.startAnimation(buttonAnimation);
                    finish();
                    break;
            }
        }
    };
}