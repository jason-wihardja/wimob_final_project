package com.wimob.wirelesskite;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

public class SignUpActivity extends ActionBarActivity {

    TextView title;
    Button signUpButton;
    EditText usernameEditText, emailEditText, telephoneEditText, passwordEditText;

    ParseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().hide();

        setContentView(R.layout.activity_signup);

        title = (TextView) findViewById(R.id.kiteStringTitle_signup);
        title.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "OpenSans-Light.ttf"));

        signUpButton = (Button) findViewById(R.id.signUpButton_signup);
        signUpButton.setOnClickListener(signUpButtonClicked);

        usernameEditText = (EditText) findViewById(R.id.usernameEditText_signup);
        emailEditText = (EditText) findViewById(R.id.emailEditText_signup);
        telephoneEditText = (EditText) findViewById(R.id.telephoneEditText_signup);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText_signup);
    }

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void doSignUp() {
        user = new ParseUser();
        user.setUsername(usernameEditText.getText().toString());
        user.setPassword(passwordEditText.getText().toString());
        user.setEmail(emailEditText.getText().toString());
        user.put("phone", telephoneEditText.getText().toString());

        final ProgressDialog progressDialog = ProgressDialog.show(this, null, "Singing up...", true, false);

        user.signUpInBackground(new SignUpCallback() {

            @Override
            public void done(ParseException e) {
                progressDialog.dismiss();

                if (e == null) {
                    proceedWithSignUp();
                } else {
                    displayError(e);
                }
            }
        });
    }

    public void proceedWithSignUp() {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, "Preparing app for first time usage...", true, false);

        final ParseObject userPreference = new ParseObject("Preferences");
        userPreference.put("user_id", ParseUser.getCurrentUser());
        userPreference.put("check_interval", 15);
        userPreference.put("inactive_interval", 5);
        userPreference.put("emergency_message", "Hi #ContactName, this is #AppName sending you alert on behalf of #Username. He/she has not responded to the safe confirmation message we sent in the past #NumOfMinutes. Our app detected his/her was last heading to #LastDestination and his/her last tracked position is in #GPSData. Thank you.");
        
        /* TEST DATA */
        JSONArray contactList = new JSONArray();
        try {
            JSONObject contact1 = new JSONObject();
            contact1.put("name", "Contact One");
            contact1.put("number", "1111111111");
            contactList.put(contact1);
            
            JSONObject contact2 = new JSONObject();
            contact2.put("name", "Contact Two");
            contact2.put("number", "2222222222");
            contactList.put(contact2);
            
            JSONObject contact3 = new JSONObject();
            contact3.put("name", "Contact Three");
            contact3.put("number", "3333333333");
            contactList.put(contact3);
        } catch (JSONException e) {
            Log.e("SIGNUP_ACTIVITY", e.getMessage());
        }
        
        JSONArray commonPlaces = new JSONArray();
        try {
            JSONObject place1 = new JSONObject();
            place1.put("name", "Monumen Nasional (Monas)");
            place1.put("placeQuery", "monas");
            place1.put("zoomLevel", 16);
            commonPlaces.put(place1);
            
            JSONObject place2 = new JSONObject();
            place2.put("name", "Eiffel Tower");
            place2.put("placeQuery", "Eiffel Tower");
            place2.put("zoomLevel", 16);
            commonPlaces.put(place2);
        } catch (JSONException e) {
            Log.e("SIGNUP_ACTIVITY", e.getMessage());
        }
        /* TEST DATA */
        
        userPreference.put("emergency_contact_list", contactList);
        userPreference.put("common_places", commonPlaces);

        userPreference.saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {
                progressDialog.dismiss();
                
                if (e == null) {
                    Intent intent = new Intent(getApplicationContext(), ProgramActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    displayError(e);
                }
            }
        });
    }

    public void linkPhotoWithUser() {
        // TODO Implement add user photo
    }

    private void displayError(ParseException e) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Error!");
        alert.setIcon(R.drawable.ic_action_error);

        String message = e.getMessage();
        message = Character.toUpperCase(message.charAt(0)) + message.substring(1);
        alert.setMessage(message);

        alert.setNeutralButton("OK", null);
        alert.show();
    }

    OnClickListener signUpButtonClicked = new OnClickListener() {
        @Override
        public void onClick(View view) {
            Boolean errorExists = false;

            // Form Validation
            if (usernameEditText.getText().toString().trim().length() == 0) {
                usernameEditText.setError(getString(R.string.username_empty_error_message));
                errorExists = true;
            } else usernameEditText.setError(null);

            if (emailEditText.getText().toString().trim().length() == 0) {
                emailEditText.setError(getString(R.string.email_empty_error_message));
                errorExists = true;
            } else emailEditText.setError(null);

            if (telephoneEditText.getText().toString().trim().length() == 0) {
                telephoneEditText.setError(getString(R.string.telephone_empty_error_message));
                errorExists = true;
            } else telephoneEditText.setError(null);

            if (passwordEditText.getText().toString().trim().length() == 0) {
                passwordEditText.setError(getString(R.string.password_empty_error_message));
                errorExists = true;
            } else if (passwordEditText.getText().toString().trim().length() < 6) {
                passwordEditText.setError(getString(R.string.password_short_error_message));
                errorExists = true;
            } else passwordEditText.setError(null);

            // Do Action
            if (!errorExists) doSignUp();
        }
    };
}
