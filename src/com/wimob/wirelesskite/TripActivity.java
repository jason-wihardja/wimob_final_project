package com.wimob.wirelesskite;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBarActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.ParseObject;

public class TripActivity extends ActionBarActivity {

    private final int animationDuration = 2500;

    private final ParseObject userPreference = ProgramActivity.getUserPreferences();

    private MapView mapView;
    private GoogleMap googleMap;
    private int mapType;

    private String destinationName;
    private LatLng lastPosition;
    private LatLng destinationPosition;

    private Button checkInButton;

    private final CountDownTimer checkInTimer = new CountDownTimer(60 * 1000 * userPreference.getInt("check_interval"), 1000) {

        @Override
        public void onTick(long millisUntilFinished) {
            TextView timeTextView = (TextView) findViewById(R.id.timeTextView);

            Long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
            Long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));

            timeTextView.setText(String.format("%02d:%02d to next check-in", minutes, seconds));
        }

        @Override
        public void onFinish() {
            checkInButton.setVisibility(View.VISIBLE);
            emergencyTimer.start();
        }
    };

    private final CountDownTimer emergencyTimer = new CountDownTimer(60 * 1000 * userPreference.getInt("inactive_interval"), 1000) {

        @Override
        public void onTick(long millisUntilFinished) {
            TextView timeTextView = (TextView) findViewById(R.id.timeTextView);

            Long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
            Long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished));

            timeTextView.setText(String.format("%02d:%02d to sending emergency message", minutes, seconds));
        }

        @Override
        public void onFinish() {
            emergencyTimer.cancel();
            sendEmergencyMessage();

            System.gc();
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip);

        getSupportActionBar().hide();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (savedInstanceState != null) mapType = savedInstanceState.getInt("mapType");
        else mapType = GoogleMap.MAP_TYPE_HYBRID;

        this.destinationPosition = getLatLngFromString(getIntent().getExtras().getString("coordinate"));

        this.destinationName = getIntent().getExtras().getString("destination");
        TextView tripTextView = (TextView) findViewById(R.id.tripTextView);
        tripTextView.setText(destinationName);

        TextView timeTextView = (TextView) findViewById(R.id.timeTextView);
        timeTextView.setText("05:00 to next check-in");

        checkInButton = (Button) findViewById(R.id.checkInButton);
        checkInButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                checkInButton.setVisibility(View.GONE);
                emergencyTimer.cancel();
                checkInTimer.start();
            }
        });
        checkInButton.setVisibility(View.GONE);

        Button roadMapButton = (Button) findViewById(R.id.roadButton);
        roadMapButton.setOnClickListener(mapTypeButtonClicked);

        Button hybridButton = (Button) findViewById(R.id.satButton);
        hybridButton.setOnClickListener(mapTypeButtonClicked);

        Button terrainButton = (Button) findViewById(R.id.terrButton);
        terrainButton.setOnClickListener(mapTypeButtonClicked);

        mapView = (MapView) findViewById(R.id.currentMap);
        mapView.onCreate(savedInstanceState);
        MapsInitializer.initialize(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setupMapIfNeeded();
    }

    private void setupMapIfNeeded() {
        if (googleMap == null) {
            googleMap = mapView.getMap();
            if (googleMap != null) setupMap();
        }
    }

    private void setupMap() {
        googleMap.setMyLocationEnabled(true);
        googleMap.setBuildingsEnabled(true);
        googleMap.setIndoorEnabled(false);
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.setMapType(mapType);
        googleMap.setPadding(0, (int) getResources().getDimension(R.dimen.navigation_max_height), 0, (int) getResources().getDimension(R.dimen.button_bar_height));
        googleMap.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {

            @Override
            public void onMyLocationChange(Location arg0) {
                lastPosition = new LatLng(arg0.getLatitude(), arg0.getLongitude());

                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(lastPosition);
                builder.include(destinationPosition);

                googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), (int) getResources().getDimension(R.dimen.padding_20dp)), animationDuration, null);
            }
        });
        googleMap.setOnMapLoadedCallback(new OnMapLoadedCallback() {

            @Override
            public void onMapLoaded() {
                moveToMyLocation();
            }
        });

        googleMap.addMarker(new MarkerOptions().position(destinationPosition).title("Destination").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).draggable(false));
    }

    public void moveToMyLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location location = null;
        String[] providers = { LocationManager.GPS_PROVIDER, LocationManager.NETWORK_PROVIDER, LocationManager.PASSIVE_PROVIDER };
        for (String provider : providers) {
            if (!locationManager.isProviderEnabled(provider)) continue;
            if (locationManager != null) {
                location = locationManager.getLastKnownLocation(provider);
                if (location != null) break;
            }
        }

        if (location != null) {
            lastPosition = new LatLng(location.getLatitude(), location.getLongitude());

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(lastPosition);
            builder.include(destinationPosition);

            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), (int) getResources().getDimension(R.dimen.padding_20dp)), animationDuration, null);
        }
    }

    private LatLng getLatLngFromString(String query) {
        final String LatLngRegex = "([-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?))(,\\s*)([-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?))";
        final Pattern pattern = Pattern.compile(LatLngRegex);
        final Matcher matcher = pattern.matcher(query);

        if (matcher.matches()) {
            return new LatLng(Double.parseDouble(matcher.group(1)), Double.parseDouble(matcher.group(6)));
        } else {
            return null;
        }
    }

    OnClickListener mapTypeButtonClicked = new OnClickListener() {

        @Override
        public void onClick(View view) {
            if (checkInButton.getVisibility() == View.GONE) {
                int previousType = mapType;

                switch (view.getId()) {
                    case R.id.roadButton:
                        mapType = GoogleMap.MAP_TYPE_NORMAL;
                        break;

                    case R.id.satButton:
                        mapType = GoogleMap.MAP_TYPE_HYBRID;
                        break;

                    case R.id.terrButton:
                        mapType = GoogleMap.MAP_TYPE_TERRAIN;
                        break;
                }

                if (previousType != mapType) googleMap.setMapType(mapType);
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
        setupMapIfNeeded();
        mapView.onResume();

        checkInTimer.start();
    }

    private void sendEmergencyMessage() {
        String messageTemplate = userPreference.getString("emergency_message");

        JSONArray contactArray = userPreference.getJSONArray("emergency_contact_list");
        for (int i = 0; i < contactArray.length(); i++) {
            try {
                JSONObject contact = contactArray.getJSONObject(i);
                String contactName = contact.getString("name");
                String contactNumber = contact.getString("number");

                String userName = ProgramActivity.getCurrentUser().getUsername();
                Integer numOfMinutes = userPreference.getInt("check_interval") + userPreference.getInt("inactive_interval");

                String message = messageTemplate.replace("#ContactName", contactName);
                message = message.replace("#AppName", getString(R.string.app_name));
                message = message.replace("#Username", userName);
                message = message.replace("#NumOfMinutes", numOfMinutes + " minutes");
                message = message.replace("LastDestination", destinationName);
                message = message.replace("#GPSData", lastPosition.latitude + ", " + lastPosition.longitude);

                if (contactName.equals("Android Emulator")) {
                    sendSMS(message, contactNumber);
                } else {
                    Toast.makeText(this, "Number: " + contactNumber + "\n" + message, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                Log.e("EMERGENCY_MESSAGE", e.getMessage());
            }
        }

        TextView timeTextView = (TextView) findViewById(R.id.timeTextView);
        timeTextView.setText("Emergency message sent!");
    }

    private void sendSMS(String content, String number) {
        SmsManager smsManager = SmsManager.getDefault();

        ArrayList<String> messages = smsManager.divideMessage(content);
        smsManager.sendMultipartTextMessage(number, null, messages, null, null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.activity_open_animation, R.anim.activity_close_animation);
        mapView.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("mapType", mapType);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Stop Trip");
        builder.setMessage("Are you sure?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.gc();
                finish();
            }
        });
        builder.setNegativeButton("Cancel", null);

        builder.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
}
