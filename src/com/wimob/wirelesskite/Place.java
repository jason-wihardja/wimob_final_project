package com.wimob.wirelesskite;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class Place implements Comparable<Place> {

    private String name;
    private String placeQuery;
    private Integer zoomLevel;

    private String imgURL;
    private Bitmap image;

    private PlacesAdapter placesAdapter;

    public Place(String name, String placeQuery, Integer zoomLevel) {
        this.name = name;
        this.placeQuery = placeQuery;
        this.zoomLevel = zoomLevel;

        this.imgURL = "http://maps.googleapis.com/maps/api/staticmap?size=640x375&scale=2&sensor=false&format=jpg&maptype=hybrid&markers=color:green";
        this.imgURL += URLEncoder.encode("|");
        this.imgURL += URLEncoder.encode(placeQuery);
        this.imgURL += "&center=";
        this.imgURL += URLEncoder.encode(placeQuery);
        this.imgURL += "&zoom=";
        this.imgURL += URLEncoder.encode(zoomLevel.toString());
        this.imgURL += "&key=AIzaSyAqgLmcPE_iUB3Prq9Vu1TvUHQwoNwssyw";

        this.image = null;
    }

    public Place(Context context, JSONObject object) {
        try {
            this.name = object.getString("name");
            this.placeQuery = object.getString("placeQuery");
            this.zoomLevel = object.getInt("zoomLevel");

            this.imgURL = "http://maps.googleapis.com/maps/api/staticmap?size=640x375&scale=2&sensor=false&format=jpg&maptype=hybrid&markers=color:green";
            this.imgURL += URLEncoder.encode("|");
            this.imgURL += URLEncoder.encode(object.getString("placeQuery"));
            this.imgURL += "&center=";
            this.imgURL += URLEncoder.encode(object.getString("placeQuery"));
            this.imgURL += "&zoom=";
            this.imgURL += URLEncoder.encode(object.getString("zoomLevel"));
            this.imgURL += "&key=AIzaSyAqgLmcPE_iUB3Prq9Vu1TvUHQwoNwssyw";

            this.image = null;
        } catch (JSONException e) {
            Toast.makeText(context, "Failed to retrieve common places list.", Toast.LENGTH_SHORT).show();
        }
    }
    
    public JSONObject getJSONObject(Context context) {
        JSONObject result = null;
        
        try {
            result = new JSONObject();
            result.put("name", this.name);
            result.put("placeQuery", this.placeQuery);
            result.put("zoomLevel", this.zoomLevel);
        } catch (JSONException e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaceQuery() {
        return placeQuery;
    }

    public void setPlaceQuery(String placeQuery) {
        this.placeQuery = placeQuery;
    }

    public Integer getZoomLevel() {
        return zoomLevel;
    }

    public void setZoomLevel(Integer zoomLevel) {
        this.zoomLevel = zoomLevel;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
    
    public void recycleImage() {
        image.recycle();
        image = null;
    }

    public PlacesAdapter getPlacesAdapter() {
        return placesAdapter;
    }

    public void setPlacesAdapter(PlacesAdapter placesAdapter) {
        this.placesAdapter = placesAdapter;
    }

    public void loadImage(PlacesAdapter placesAdapter) {
        this.placesAdapter = placesAdapter;

        if (imgURL != null && !(imgURL.trim().length() == 0)) {
            new ImageLoadTask().execute(imgURL);
        }
    }

    private class ImageLoadTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                return BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            image = result;
            placesAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public int compareTo(Place another) {
        return this.name.compareTo(another.getName());
    }
}
