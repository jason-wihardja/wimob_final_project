package com.wimob.wirelesskite;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

public class PlacesAdapter extends BaseAdapter {

    private ArrayList<Place> items;

    private LayoutInflater layoutInflater;

    public PlacesAdapter(Context context, ArrayList<Place> items) {
        layoutInflater = LayoutInflater.from(context);
        this.items = new ArrayList<Place>();
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Place place = (Place) items.get(position);

        convertView = layoutInflater.inflate(R.layout.place_entry_layout, null);
        ImageView image = (ImageView) convertView.findViewById(R.id.placeImage);
        if (place.getImage() != null) {
            image.setScaleType(ScaleType.CENTER_CROP);
            image.setImageBitmap(place.getImage());
        } else {
            image.setImageResource(R.drawable.loading);
            place.loadImage(this);
        }

        TextView name = (TextView) convertView.findViewById(R.id.placeName);
        name.setText(place.getName());
        
        TextView placeQuery = (TextView) convertView.findViewById(R.id.placeQuery);
        placeQuery.setText(place.getPlaceQuery());
        
        TextView zoomLevel = (TextView) convertView.findViewById(R.id.zoomLevel);
        zoomLevel.setText(place.getZoomLevel().toString());

        return convertView;
    }
}
